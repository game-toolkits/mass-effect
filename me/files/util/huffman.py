class BitArray(object):
	def __init__(self, data = None, length = None):
		if length is None:
			length = len(data) * 8
		len_data = self._sizeadj(length)
		if data is None:
			self._bytes = bytearray(len_data)
		elif len_data > len(data):
			raise ValueError('Size of Bit Array exceeds bits in data ({} > {} bytes)'.format(length, len(data)))
		else:
			self._bytes = (data if isinstance(data, bytearray) else bytearray(data))
		self._len = length
	def append(self, bits, length = None):
		adj = (self._len % 8)
		pos = (self._len // 8)
		pos_bits = 0
		remainder = 0
		if isinstance(bits, BitArray):
			if length is None:
				length = len(bits)
			elif length > len(bits):
				raise ValueError('Cannot append {} bits from BitArray with only {} bits'.format(length, len(bits)))
			bits = bits._bytes
		elif length is None:
			length = len(bits) * 8
		elif length > (len(bits) * 8):
			raise ValueError('Cannot append {} bits; data contains only {} bytes'.format(length, len(bits)))
		if pos < len(self._bytes):
			remainder = (self._bytes[pos] & (0xFF >> (8 - adj)))
			while ((pos < len(self._bytes)) and ((pos_bits * 8) < length)):
				self._bytes[pos] = (remainder | (0xFF & (bits[pos_bits] << adj)))
				remainder = ((bits[pos_bits] & 0xFF) >> (8 - adj))
				pos += 1
				pos_bits += 1
		while (pos_bits * 8) < length:
			self._bytes.append(remainder | (0xFF & (bits[pos_bits] << adj)))
			remainder = ((bits[pos_bits] & 0xFF) >> (8 - adj))
			pos_bits += 1
			pos += 1
		if ((length + adj) % 8) > 0:
			if pos < len(self._bytes):
				self._bytes[pos] = remainder
			else:
				self._bytes.append(remainder)
		self._len += length
		return self

	def __setitem__(self, pos, val):
		idx = pos // 8
		if idx >= len(self._bytes):
			raise IndexError('Bitfield of length {} does not cover file position 0x{:08X}'.format(len(self._bytes), pos))
		flags = self._bytes[idx]
		flag = 1 << (pos % 8)
		if val:
			if (val == True) or (val == 1):
				flags = flags | flag # bitwise-OR
			else:
				raise ValueError('Attempt to set a flag with a non-boolean value: {}'.format(val))
		else:
			flags = flags & (~flag) # AND-NOT
		self._bytes[idx] = flags

	def __getitem__(self, pos):
		if isinstance(pos, slice):
			raise NotImplementedError('No support for slice yet')
		if pos >= self._len:
			raise IndexError('Index {} beyond the range of bits ({})'.format(pos, self._len))
		flags = self._bytes[pos // 8]
		flag = 1 << (pos % 8)
		return ((flags & flag) > 0)
	def __iter__(self):
		return iter((((1 << i) & self._bytes[idx]) > 0)
			for idx in range(0, min(len(self._bytes), self._sizeadj(self._len)))
			for i in range(0, (8 if (((idx + 1) * 8) <= self._len) else (self._len % 8))))
	def __len__(self):
		return self._len
	@staticmethod
	def _sizeadj(size):
		return -((-size) // 8) # Ceiling division
	def set_size(self, size):
		size = self._sizeadj(size)
		if size < len(self._bytes):
			self._bytes = self._bytes[:size]
		elif size > len(self._bytes):
			self._bytes = self._bytes + bytes(size - len(self._bytes))
	def size_matches(self, size):
		return len(self._bytes) == self._sizeadj(size)
	@property
	def raw(self):
		return self._bytes[:self._sizeadj(self._len)]
	def __str__(self):
		return ''.join(('1' if b else '0') for b in self)

def list_to_map(tree, is_encode = True):
	stack = [(tree[-1], 0, 0)]
	m = {}
	def add_map(s, v, lvl):
		if is_encode:
			m[s] = (v, lvl)
		else:
			m[(v, lvl)] = s
	def process_node(i, v, lvl):
		if isinstance(i, int):
			stack.append((tree[i], v, lvl))
		else:
			add_map(i, v, lvl)
	while stack:
		(l, r), v, lvl = stack.pop()
		process_node(l, v               , (lvl + 1))
		process_node(r, (v + (1 << lvl)), (lvl + 1))
	return m

def decode_str(huffmap, bits, pos = 0, len_max = -1):
	s = ''
	c = 0
	while (c is not None):
		v = 0
		l = 0
		while True:
			if pos >= len(bits):
				raise IndexError('Attempt to read position {}, beyond the bounds of the Huffman bits ({})'.format(pos, len(bits)))
			if bits[pos]:
				v += (1 << l)
			l += 1
			pos += 1
			c = huffmap.get((v, l), 0)
			if (c != 0):
				if (c is None):
					return s
				break
		s += c
		if (len_max >= 0) and (len(s) >= len_max):
			return s

def encode_str(huffmap, s):
	bits = BitArray(bytearray(len(s)), 0)
	def append_c(c):
		try:
			v, l = huffmap[c]
		except KeyError:
			raise KeyError('Huffman Tree does not contain value for key: ' + ('(None)' if (c is None) else str(c)))
		bits.append(bytes(((0xFF & v), (0xFF & (v >> 8)), (0xFF & (v >> 16)), (0xFF & (v >> 24)))), l)
	for c in s:
		append_c(c)
	append_c(None) # Value terminator
	return bits

