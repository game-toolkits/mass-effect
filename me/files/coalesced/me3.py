import struct
import json
from me.common.util import LazyFormatter
from me.files.util import crc32, huffman
from .base import (CoalescedFile as __CBase, FileEntry as __EBase
	, VersionError, DataError, Log, OrderedDict)

class FileEntry(OrderedDict, __EBase):
	@classmethod
	def from_bytes(cls, data):
		o = json.loads(data.decode('utf-8'), object_pairs_hook = cls) # Maintain the order
		for values, value_name, vs in ((values, value_name, vs)
				for (section_name, values) in o.items()
				for (value_name, vs) in values.items()):
			values[value_name] = Values(Value(v) for v in (vs if isinstance(vs, list) else [vs]))
		return o
	def to_bytes(self):
		return json.dumps(self, indent='\t', cls = Values.JSONEncoder).encode('utf-8')

class Values(object):
	class JSONEncoder(json.JSONEncoder):
		def default(self, o):
			return ((o._l[0] if (len(o) == 1) else o._l) if isinstance(o, Values)
				else (o.value if isinstance(o, Value) else super().default(o)))
	def __init__(self, it):
		self._l = list(it)
	def __len__(self):
		return len(self._l)
	def __iter__(self):
		return iter(self._l)
	def append(v):
		return self._l.append(v)
class Value(object):
	@classmethod
	def __new__(cls, val, *args, **kwargs):
		return (None if (val is None) else super().__new__(cls))
	def __init__(self, val, huffmap = None, huffbits = None):
		if isinstance(val, dict):
			if len(val) < 1:
				raise ValueError('No values in Value map')
			elif len(val) > 1:
				raise ValueError('Value map with more than one value: ' + str(len(val)))
			op, val = next(kv for kv in val.items())
			self.list_add = (op != 'remove')
			if self.list_add and (op != 'add'):
				raise ValueError('Invalid directive in Value map: ' + str(op))
		else:
			self.list_add = None
		if huffmap is None:
			self._val = val
		else:
			self._val = None
			self._huff = (huffmap, huffbits, val)
	@property
	def value(self):
		return (self._value() if (self.list_add is None)
			else {('add' if self.list_add else 'remove'): self._value()})
	def _value(self):
		if self._val is None:
			self._val = huffman.decode_str(*self._huff)
			del self._huff
		return self._val
	def __str__(self):
		return str(self._value())

class CoalescedFile(__CBase):
	version = 'Mass Effect 3'
	FileEntry = FileEntry
	hufftree = ()
	@classmethod
	def load(cls, f):
		header = f.peek(8)
		if header[:4] == b'fmrm':
			fmt_int = '>I'
			fmt_short = '>H'
		elif header[:4] == b'mrmf':
			fmt_int = '<I'
			fmt_short = '<H'
		else:
			raise VersionError('Invalid magic header value: {}', header[:4].hex())

		def read_int():
			return struct.unpack(fmt_int, f.read(4))[0]
		def read_short():
			return struct.unpack(fmt_short, f.read(2))[0]

		version = struct.unpack(fmt_int, header[4:])[0]
		if version != 1:
			raise VersionError('Invalid version in header: {}', hex(version))

		f.seek(8) # Skip the header now that we've validated it
		key_len_max = read_int()
		value_len_max = read_int()
		size_strings = read_int()
		size_huffman = read_int()
		size_index = read_int()
		size_data = read_int()
		Log.info('{}', LazyFormatter(None, (lambda:
			'Recognized valid ME3 Coalesced file ({} Endian)\n{}'.format(('Big' if (fmt_int == '>I') else 'Little')
				, '\n'.join('\t{:20}: 0x{:08X}'.format(k, v) for (k, v) in (
					('Max Key Length', key_len_max),
					('Max Value Length', value_len_max),
					('Size (Strings)', size_strings),
					('Size (Huffman)', size_huffman),
					('Size (Index)', size_index),
					('Size (Data)', size_data),
				))
			))))

		offset_strings = f.tell()
		size_strings_local = read_int()
		if size_strings != size_strings_local:
			raise DataError('Mismatch between String size from Header ({}) and Local String Table ({})', size_strings, size_strings_local)

		def check_pos(pos_new, section, ret = None):
			if f.tell() < pos_new:
				Log.info('Advancing file position for {}, from {:08X} to {:08X}', section, f.tell(), pos_new)
				f.seek(pos_new)
			elif f.tell() > pos_new:
				raise DataError('Attempt to move file position backward for {}, from {:08X} to {:08X}', section, f.tell(), pos_new)
			return (f.tell() if (ret is None) else ret)

		count_strings = read_int()
		offset_strings_internal = f.tell()
		string_hashes = [(read_int(), read_int()) for i in range(0, count_strings)]
		def read_string(offset, h):
			check_pos(offset_strings_internal + offset, 'String')
			len_string = read_short()
			data = f.read(len_string)
			h_data = crc32.calculate(data)
			if h != h_data:
				raise DataError('Mismatch between CRC values ({:08X} != {:08X}) for String at position {:08X}: {}'
					, h, h_data, (offset_strings + offset), data[:99])
			return data.decode('ascii')
		strings = [read_string(o, h) for (h, o) in string_hashes]
		Log.debug('Successfully read {} Strings', len(strings))

		offset_huffman = check_pos(offset_strings + size_strings, 'Huffman Tree')
		def node_value():
			v = read_int()
			if v < 0x80000000:
				return v
			v = 0xFFFFFFFF - v
			return (bytes(((v & 0xFF), ((v >> 8) & 0xFF))).decode('utf-16-le') if (v > 0) else None)
		get_list_by_length = (lambda *args: [tuple(fx() for fx in args) for _ in range(0, read_short())])
		hufftree = get_list_by_length(node_value, node_value)
		huffmap = huffman.list_to_map(hufftree, False)
		Log.debug('Huffman Tree:\n{}', LazyFormatter(huffmap
			, (lambda m: '\n'.join('\t{}: {}'.format(k, v) for (k, v) in m.items()))))

		get_key_offset = (lambda: get_list_by_length((lambda: strings[read_short()]), read_int))
		offset_index = check_pos(offset_huffman + size_huffman, 'Indices')
		files = [(check_pos((offset_index + file_offset), ('Index for File: ' + file_name), file_name)
			, [(check_pos((offset_index + file_offset + section_offset), ('Index of Section: ' + section_name), section_name)
				, [(check_pos((offset_index + file_offset + section_offset + val_offset), ('Index of Value: ' + val_name), val_name)
						, get_list_by_length(read_int)
					) for (val_name, val_offset) in get_key_offset()]
				) for (section_name, section_offset) in get_key_offset()]
			) for (file_name, file_offset) in get_key_offset()]

		check_pos(offset_index + size_index, 'Huffman Data')
		count_bits = read_int()
		huffbits = huffman.BitArray(f.read(size_data), count_bits)

		def process_val(v, fmt, *args):
			type_val = (v & 0xE0000000) >> 29
			if type_val == 1:
				return None
			elif (type_val < 1) or (type_val > 4):
				raise DataError('Unknown type for value: 0x{:08X}: {} (' + fmt + ')', v, type_val, *args)
			val = (v & 0x1FFFFFFF)
			return (val if (type_val < 3) else {('remove' if (type_val > 3) else 'add'): val})
		cf = cls(((name, FileEntry(
			(section_name,
				OrderedDict((value_name, Values(
					Value(process_val(v, '{}:{}:{}', name, section_name, value_name), huffmap, huffbits)
						for v, in l)
				) for (value_name, l) in values)
			) for (section_name, values) in sections)
		) for (name, sections) in files))

		cf.hufftree = hufftree
		return cf

	def save(self, f_out):
		fmt_int = '<I'
		fmt_short = '<H'
		fmt_idxoffset = '<HI'
		def pack_int(v):
			return struct.pack(fmt_int, v)
		def pack_short(v):
			return struct.pack(fmt_short, v)
		def pack_idxoffset(idx, offset):
			return struct.pack(fmt_idxoffset, idx, offset)

		phases = ('', 'Gathering all values from Files', 'Folding values', 'Huffman encoding')
		PHASE_GATHER = 1
		PHASE_FOLD = 2
		PHASE_ENCODE = 3

		self.progress.set_indices(phases, len(self.files))
		self.progress.start()

		names = set()
		values = []
		for i, (name_file, fe) in enumerate(self.files.items()):
			self.progress.update(PHASE_GATHER, i)
			names.add(name_file)
			for name_section, section in fe.items():
				names.add(name_section)
				for name_value, vlist in section.items():
					names.add(name_value)
					for v in vlist:
						if v is not None:
							values.append(str(v))

		# Mass Effect requires the Key Index to follow the order of the CRC32 values
		names = sorted(names, key=(lambda v: crc32.calculate(v.encode('ascii'))))
		map_names = dict((n, i) for i, n in enumerate(names))

		# Mass Effect does not require ordering the Values, but we do it so that
		# 	we can identify and collapse substrings for the Huffman encoding
		values.sort(key = (lambda v: len(v)), reverse = True)
		if values:
			Log.debug('Package has {} values, from size {} to {}'
				, len(values), len(values[-1]), len(values[0]))

		substrings = []
		self.progress.set_indices(phases, len(values))
		for i, v in enumerate(values):
			self.progress.update(PHASE_FOLD, i)
			try:
				l = next(s for s in substrings if s[0].endswith(v))
				if v not in l:
					l.append(v)
			except StopIteration:
				substrings.append([v])
		size_values = sum(len(l[0]) for l in substrings)
		Log.info('Encoding {} unique values out of {} total ({} chars)'
			, len(substrings), len(values), size_values)

		huffbits = huffman.BitArray(bytearray(min(size_values, (1 << 22))), 0) # Initial buffer, max of 4 MiB
		huffmap = huffman.list_to_map(self.hufftree, True)
		huffman_index = {}
		max_len_value = 0

		self.progress.set_indices(phases, len(substrings))
		for i, l in enumerate(substrings):
			index_end = 0
			self.progress.update(PHASE_ENCODE, i)
			for v in l:
				bits = huffman.encode_str(huffmap, v)
				if index_end > 0:
					idx = (index_end - len(bits))
				else:
					idx = len(huffbits)
					huffbits.append(bits)
					index_end = len(huffbits)
				if idx > 0x1FFFFFFF:
					raise IndexError('Index into Huffman data greater than maximum size: 0x{:08X}'.format(idx))
				huffman_index[v] = idx
			if len(l[0]) > max_len_value:
				max_len_value = len(l[0])
				

		def get_chunks(nvs, get_chunks_inner):
			offset = (2 + (6 * len(nvs)))
			indices = [pack_short(len(nvs))]
			chunks = [None]
			for name, o in nvs.items():
				idx_name = map_names[name]
				indices.append(pack_idxoffset(idx_name, offset))
				chunk = b''.join(get_chunks_inner(o))
				chunks.append(chunk)
				offset += len(chunk)
			chunks[0] = b''.join(indices)
			return chunks

		def get_chunks_values(vs):
			nonlocal max_len_value
			v_datas = [pack_short(len(vs))]
			for v in vs:
				if v is None:
					v = (1 << 29)
				else:
					v = (((2 if (v.list_add is None) else (3 if v.list_add else 4)) << 29)
						| (0x1FFFFFFF & huffman_index[str(v)]))
				v_datas.append(pack_int(v))
			return v_datas

		file_datas = get_chunks(self.files
			, (lambda fe: get_chunks(fe
				, (lambda values: get_chunks(values
					, (lambda vs: get_chunks_values(vs))
		)))))

		string_datas = [bytes(4), pack_int(len(names)), None]
		string_hashes = []
		offset_string = (8 * len(names))
		for s in names:
			data = s.encode('ascii')
			string_hashes.append(pack_int(crc32.calculate(data)) + pack_int(offset_string))
			string_datas.append(pack_short(len(data)))
			string_datas.append(data)
			offset_string += (2 + len(data))
		string_datas[2] = b''.join(string_hashes)
		size_strings = sum(len(data) for data in string_datas)
		string_datas[0] = pack_int(size_strings)

		def val_for_node(v):
			return (v if isinstance(v, int)
				else (0xFFFFFFFF if (v is None)
					else (0xFFFFFFFF - struct.unpack('<H', (v.encode('utf-16-le') + bytes(2))[:2])[0])
			))
		data_hufftree = b''.join([pack_short(len(self.hufftree))]
			+ [(pack_int(val_for_node(l)) + pack_int(val_for_node(r))) for (l, r) in self.hufftree])

		f_out.write(b'mrmf')
		for v in (1
			, max(len(s) for s in names)
			, max_len_value
			, size_strings
			, len(data_hufftree)
			, sum(len(data) for data in file_datas)
			, len(huffbits.raw)
		):
			f_out.write(pack_int(v))
		for data in string_datas:
			f_out.write(data)
		f_out.write(data_hufftree)
		for data in file_datas:
			f_out.write(data)
		f_out.write(pack_int(len(huffbits)))
		f_out.write(huffbits.raw)
		self.progress.clear()

