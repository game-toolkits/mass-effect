from .base import CoalescedFile as __CBase, DataError

class CoalescedFile(__CBase):
	version = 'Mass Effect 2'

	def save(self, f):
		def write_int(v):
			f.write(bytes(((0xFF & v), (0xFF & (v >> 8)), (0xFF & (v >> 16)), (0xFF & (v >> 24)))))
		write_int(len(self.files) * 2)
		for v in (v for (n, fe) in self.files.items() for v in (n.encode('ascii'), fe.to_bytes())):
			data = v + bytes(1) # Add NULL terminator
			write_int(len(data))
			f.write(data)

	@classmethod
	def load(cls, f):
		def read_int():
			data = f.read(4)
			return (data[0] | (data[1] << 8) | (data[2] << 16) | (data[3] << 24))
		def validate_chunk(count):
			data = f.read(count)
			if (not data) or (data[-1] != 0):
				raise DataError(('Coalesced data entries must terminate with the NULL (0) byte '
					+ '(expected at position 0x{:08X})'), f.tell())
			return data[:-1]
		count_chunks = read_int()
		if ((count_chunks % 2) > 0):
			raise DataError(('Coalesced data requires a name to correspond to each file data block ' +
				'(so length must not contain an odd value: {})'), count_chunks)
		return cls((validate_chunk(read_int()).decode('ascii'), cls.FileEntry(validate_chunk(read_int())))
			for _ in range(0, count_chunks, 2))

