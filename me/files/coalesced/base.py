from collections import OrderedDict
from me.common.util import Log, UnseekableReader, ExceptionWithFormat
from me.common.progress import ProgressTracker

class DataError(ExceptionWithFormat, ValueError):
	pass
class VersionError(DataError):
	pass

class FileEntry(object):
	def __init__(self, data):
		self._bytes = (data or b'')
	def to_bytes(self):
		return self._bytes
	@classmethod
	def from_bytes(cls, data):
		return cls(data)

class CoalescedFile(object):
	FileEntry = FileEntry
	version = 'Abstract'
	def __init__(self, files = None, progress = None):
		self.files = (OrderedDict() if (files is None) else
			(files if hasattr(files, 'keys') else OrderedDict(files)))
		self.progress = (progress or ProgressTracker())

	@classmethod
	def load(cls, f_in):
		f = UnseekableReader(f_in)
		for c in cls.types:
			try:
				if c.load != cls.load:
					return c.load(f)
			except VersionError as ex:
				Log.debug(str(ex))
		raise VersionError('Data does not match any Coalesced version')
	def save(self, f_out):
		raise NotImplementedError('{} version of a Coalesced file does not support save'.format(self.version))

	class __TypeSelector(object):
		def __init__(self):
			self._d = OrderedDict()
		def __getitem__(self, key):
			try:
				return self._d[key[-1:]]
			except KeyError:
				raise KeyError(key)
		def __setitem__(self, key, v):
			self._d[key[-1:]] = v
		def __iter__(self):
			return iter(self._d.values())
	types = __TypeSelector()

from .me3 import CoalescedFile as __ME3
CoalescedFile.types['3'] = __ME3
from .me2 import CoalescedFile as __ME2
CoalescedFile.types['2'] = __ME2

