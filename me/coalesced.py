import sys
sys.path.insert(0, ((sys.path[0] + '/..') if sys.path[0] else '..'))
from me.common.util import Log
from me.files.coalesced import CoalescedFile, DataError
from me.common.progress import TextProgressTracker

def _match_key(d, k):
	try:
		return (k, d[k]) # Try direct first
	except KeyError:
		pass

	s = k.upper()
	for name_selector in (
		(lambda n: (n.split('\\')[-1] == s)),
		(lambda n: (n.split('\\')[-1].split('.')[0] == s)),
	):
		for n in d:
			if name_selector(n.upper()):
				return (n, d[n])
	raise KeyError('Failed to find key: ' + str(k))

def __main():
	import argparse
	parser = argparse.ArgumentParser(description="""Manipulate a Mass Effect configuration package""")
	parser.add_argument(dest='file_main', metavar='FILE', help="""Operate on FILE (default to STDIN/STDOUT)""")
	parser.add_argument('-o', '--output', dest='file_out', metavar='FILE', help="""Output to FILE (default to STDOUT)""")
	parser.add_argument('-i', '--input', dest='file_in', metavar='FILE', help="""Input from FILE (default to STDIN)""")
	parser.add_argument('-c', '--create', dest='create_version', metavar='VERSION', help="""Create package for VERSION (e.g. "2", "ME2", "Mass Effect 2")""")
	parser.add_argument('-l', '--list', dest='list_show', action='store_true', help="""List files in package""")
	parser.add_argument('-x', '--extract', dest='name_extract', metavar='NAME', help="""Extract file with NAME from package""")
	parser.add_argument('-u', '--update', dest='name_update', metavar='NAME', help="""Update file with NAME in package""")
	parser.add_argument('-v', '--verbose', dest='verbose', action='count'
		, help="""Increase logging verbosity (specify multiple times for even greater verbosity)""")

	args = parser.parse_args()
	Log.basicConfig(stream = sys.stderr, level = ((Log.DEBUG if (args.verbose > 1) else Log.INFO) if args.verbose else Log.WARNING))
	path_in = ((None if ((args.file_in is None) or (args.update is not None)) else args.file_in)
		if (args.file_main is None) else args.file_main)

	cf = None
	progress = TextProgressTracker(sys.stderr, None, 0.1)
	try:
		if (path_in is not None) or (args.create_version is None):
			with (sys.stdin.buffer if (path_in is None) else open(path_in, 'rb')) as f_in:
				cf = CoalescedFile.load(f_in)
				cf.progress = progress
	except OSError as ex:
		import errno
		if (args.update is None) or (args.create_version is None) or (ex.errno != errno.ENOENT):
			raise
	except ValueError as ex:
		Log.error('{}', ex)
		Log.debug('', exc_info = ex)
		return False
	if (cf is None):
		cf = CoalescedFile.types[args.create_version](progress = progress)
		if (args.update is not None):
			cf.files[args.update] = cf.FileEntry()

	if args.list_show:
		for name in cf.files:
			print(name)
		return True

	do_write = None
	if (args.name_extract is not None):
		n, v = _match_key(cf.files, args.name_extract)
		Log.debug('Found File: {}', n)
		do_write = (lambda f: f.write(v.to_bytes()))
	elif (args.name_update is not None):
		n, v = _match_key(cf.files, args.name_update)
		with (sys.stdin.buffer if (args.file_in is None) else open(args.file_in, 'rb')) as f:
			cf.files[n] = cf.FileEntry.from_bytes(f.read())
		do_write = (lambda f: cf.save(f))
		if (args.file_out is None) and (args.file_main is not None):
			args.file_out = args.file_main

	try:
		if do_write:
			with (sys.stdout.buffer if (args.file_out is None) else open(args.file_out, 'wb')) as f_out:
				do_write(f_out)
	except:
		Log.exception('')
		if not sys.flags.interactive:
			sys.exit(1)
	return cf

if __name__ == '__main__':
	cf = __main()
	if not cf:
		sys.exit(1)

