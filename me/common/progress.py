class ProgressTracker(object):
	class _ProgressIndex(object):
		def __init__(self, idx):
			self.tot = (idx if isinstance(idx, int) else len(idx))
			self.strs = (None if isinstance(idx, int) else idx)
	def set_indices(self, *indices):
		self.indices = [self._ProgressIndex(i) for i in indices]
	def update(self, *positions):
		pass
	def start(self):
		pass
	def clear(self):
		pass

class TextProgressTracker(ProgressTracker):
	def __init__(self, buf, fmt = None, interval_update = 0, indices = None):
		self._buf = buf
		self.fmt = fmt
		self._fmt_cur = fmt
		self._len_last = 0
		self._interval = interval_update
		if interval_update > 0:
			import time
			self._get_now = time.time
		self._time_last = 0
		if indices is not None:
			self.set_indices(*indices)

	def set_indices(self, *indices):
		super().set_indices(*indices)
		get_strs = (lambda p, i, back: ('' if (p.strs is None)
			else '{}{{strs[{}]}}{}'.format(('' if back else ' '), i, (' ' if back else ''))))
		if self.fmt is None:
			self._fmt_cur = (''.join('({{pos[{i}]:>{len_tot}}}/{tot}){title}: '.format(i = i
						, len_tot = len(str(p.tot - 1))
						, tot = (p.tot - 1)
						, title = get_strs(p, i, False))
					for (i, p) in enumerate(self.indices[:-1]))
				+ '{}{{pos[{}]:>{}}}/{}'.format(get_strs(self.indices[-1], (len(self.indices) - 1), True)
					, (len(self.indices) - 1), len(str(self.indices[-1].tot)), self.indices[-1].tot))

	def start(self):
		if not self.indices:
			raise IndexError('No Progress Indices to track')
	def clear(self):
		self._buf.write('\r')
		self._buf.write(' ' * self._len_last)
		self._buf.write('\r')

	def update(self, *positions):
		if (self._interval > 0):
			now = self._get_now()
			if (now > (self._time_last + self._interval)):
				self._time_last = now
			else:
				return

		s = self._fmt_cur.format(pos = positions
			, strs = [('' if (self.indices[i].strs is None) else self.indices[i].strs[pos])
				for (i, pos) in enumerate(positions)])
		self._buf.write(s)
		if len(s) < self._len_last:
			self._buf.write(' ' * (self._len_last - len(s)))
		self._buf.write('\r')
		self._len_last = len(s)
		self._buf.flush()

