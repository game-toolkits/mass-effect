import sys
import logging
import types

def eprint(*msgs):
	for s in msgs:
		sys.stderr.write(s + '\n')

def eprint_indent(level, *msgs):
	for s in msgs:
		eprint('\t' * level + s)

class Log(object):
	@staticmethod
	def get_logger(name = None):
		log = logging.getLogger(name)
		if not hasattr(log, '_fstrings'):
			def get_message(record):
				msg = str(record.msg)
				args = record.args
				return (msg.format(*args if isinstance(args, tuple) else args) if args else msg)
			def wrap_handle(fx):
				def handle(record):
					record.getMessage = types.MethodType(get_message, record)
					return fx(record)
				return handle
			log.handle = wrap_handle(log.handle)
		log._fstrings = True
		return log

	DEBUG = logging.DEBUG
	INFO = logging.INFO
	WARNING = logging.WARNING
	ERROR = logging.ERROR
	CRITICAL = logging.CRITICAL

	@staticmethod
	def basicConfig(*args, **kwargs):
		logging.basicConfig(*args, **kwargs)
	@classmethod
	def debug(cls, *args, **kwargs):
		cls._logger_default.debug(*args, **kwargs)
	@classmethod
	def info(cls, *args, **kwargs):
		cls._logger_default.info(*args, **kwargs)
	@classmethod
	def warn(cls, *args, **kwargs):
		cls._logger_default.warning(*args, **kwargs)
	@classmethod
	def warning(cls, *args, **kwargs):
		cls._logger_default.warning(*args, **kwargs)
	@classmethod
	def error(cls, *args, **kwargs):
		cls._logger_default.error(*args, **kwargs)
	@classmethod
	def exception(cls, *args, **kwargs):
		cls._logger_default.exception(*args, **kwargs)
	@classmethod
	def critical(cls, *args, **kwargs):
		cls._logger_default.critical(*args, **kwargs)
	@classmethod
	def setLevel(cls, *args, **kwargs):
		cls._logger_default.setLevel(*args, **kwargs)

Log._logger_default = Log.get_logger('me')

class HandlesList(object):
	handles = () # Default to empty, static collection
	def __init__(self):
		self.handles = [] # But for instances that don't override __init__, provide modifiable collection
	def __enter__(self):
		return self
	def __exit__(self, *e):
		for o in (o for o in self.handles if o):
			with o:
				pass # Do nothing but ensure we call the __exit__ method
		self.handles = []

class LazyFormatter(object):
	def __init__(self, o, fx):
		self._obj = o
		self._fx = fx
	def __str__(self):
		return str(self._fx() if self._obj is None else self._fx(self._obj))

class ExceptionWithFormat(Exception):
	def __init__(self, message, *args, **kwargs):
		self._message = message
		self._args = args
		self._kwargs = kwargs
	@property
	def message(self):
		return self._message.format(*self._args, **self._kwargs)
	def call_format(self, fx):
		return fx(self._message, *self._args, **self._kwargs)
	def __str__(self):
		return self.message

class UnseekableWrapper(object):
	def __init__(self, fd):
		self.pos = 0
		self.fd = fd
		self.buf = bytes()
	def read(self, length):
		if self.buf:
			if len(self.buf) < length:
				data = self.buf + self.fd.read(length - len(self.buf))
				self.buf = bytes()
			else:
				data = self.buf[:length]
				self.buf = self.buf[length:]
		else:
			data = self.fd.read(length)
		self.pos = self.pos + len(data)
		return data
	def write(self, data):
		count = self.fd.write(data)
		self.pos = self.pos + count
		return count
	def tell(self):
		return self.pos
	def seek(self, pos, whence = 0): # Replace default seek method
		if whence > 0:
			fd.seek(pos, whence)
			return
		assert pos >= self.pos
		count = (pos - self.pos)
		while (count > 0):
			ln = self._seek(count if (count < 4096) else 4096)
			if ln <= 0:
				raise IOError('Reached end of file (0x{:08X}) trying to seek to posision: 0x{:08X}'.format(self.tell(), pos))
			count = (count - ln)
	#@abstractmethod - Future Python feature
	def _seek(self, count):
		raise NotImplementedError # abstract
	def fileno(self):
		return self.fd.fileno()
	def __enter__(self):
		return self
	def __exit__(self, *args):
		pass # No context to manage

class UnseekableReader(UnseekableWrapper):
	def _seek(self, count):
		return len(self.read(count))
	def peek(self, count):
		if count < len(self.buf):
			return self.buf[:count]
		b = self.fd.read(count - len(self.buf))
		self.buf = self.buf + b
		return self.buf
class UnseekableWriter(UnseekableWrapper):
	def _seek(self, count):
		return self.write(bytes(count))


